Scripts for bibliography management
===================================

## Update PDF metadata from Zotero entry

    ./pdfmetadata_from_zotero.py -f 'file-to-update.pdf' DOI=10.2474/trol.7.147

or

    ./pdfmetadata_from_zotero.py -f 'file-to-update.pdf' author=Mabuchi date=2012


Implicit syntax equivalent to the above:

    ./pdfmetadata_from_zotero.py -f 'file-to-update.pdf' 10.2474/trol.7.147
    ./pdfmetadata_from_zotero.py -f 'file-to-update.pdf' Mabuchi 2012

Without a file argument, it shows the (first) item match.

This scripts queries the local zotero database (looks at `~/Zotero/zotero.sqlite`), and then calls the program `exiftool` to change metadata.


### Valid search fields

- DOI
- author (any position. May thus return multiple matches)
- firstAuthor
- editor
- bookAuthor
- date

## Requirements

- SQLite3
- python-pysqlite2 (interface for SQLite3)
- exiftool


