"""
LibreOffice writer macro to use with Zotero:
build hyperlinks from in-text citations to the target item in the document bibliography.

Originally written by user @gwyn-hopkins on Zotero forum:
https://forums.zotero.org/discussion/comment/465031/#Comment_465031
"""

import uno
import re
import json
from unidecode import unidecode

# alter this for citations divided by comma
semicolon = re.compile(r"\d\w?;")
comma = re.compile(r"\d\w?,")
colon = re.compile(r"\d\w?:")
# Sanitize author names for use as ref mark
invalid_ref_chars = re.compile(r"[\s–,'’]")

document = XSCRIPTCONTEXT.getDocument()
#logfile = uno.fileUrlToSystemPath(document.URL) + '.zothyperlink.log'


def deleteRefBookmarks():
    bookmarks = document.getBookmarks()
    bmList = []
    for bookmark in bookmarks:
        if "Ref_" in bookmark.Name:
            bmList.append(bookmark.Name)
    for bm in bmList:
        bookmarks.getByName(bm).dispose()


def insertZoteroBookmarks(bNumbered):
    deleteRefBookmarks()
    vSections = document.getTextSections()
    sEventNames = vSections.getElementNames()
    for sEventName in sEventNames:
        if "ZOTERO" in sEventName:
            oSection = document.getTextSections().getByName(sEventName)
            oSectionTextRange = oSection.getAnchor()
            oPE = oSectionTextRange.createEnumeration()
            iCount = 0
            while oPE.hasMoreElements():
                oPar = oPE.nextElement()
                if oPar.supportsService("com.sun.star.text.Paragraph"):
                    iCount = iCount + 1
                    bm = document.createInstance("com.sun.star.text.Bookmark")
                    oCurs = oPar.getText().createTextCursorByRange(oPar)
                    if bNumbered:
                        bm.Name = "Ref_" + str(iCount)
                    else:
                        bm.Name = "Ref_" + findFirstWord(oCurs.getString()) + "_" + literatureDate(oCurs.getString())
                    if not document.getBookmarks().hasByName(bm.Name):
                        document.Text.insertTextContent(oCurs, bm, True)


def findFirstWord(oString):
    """Find first capitalized name"""
    notFound = True
    while notFound:
        (firstWord, nextString) = oString.split(maxsplit=1)
        if firstWord[0].isupper():
            notFound = False
        else:
            oString = nextString
    return firstWord.rstrip(",")


def literatureDate(oString):
    year = re.search(r'((19|2[0-9])\d{2}[a-z]?)', oString).group(1)
    # year numbers from 1900 to 2999 and optional a-z
    return year


def linkReferences():
    """The main macro to run. Build all hyperlinks.
    Automatically detect author-date or numbered format.

    Must be rerun every time the bibliography is refreshed by Zotero.
    """
    print("LO-zotero-hyperlink-citations: start macro linkReferences")
    search_descriptor = document.createSearchDescriptor()
    search_descriptor.SearchRegularExpression = False
    text_cursor = document.getText().createTextCursor()
    reference_marks = document.ReferenceMarks

    ref_count = 0
    for mark_count, reference_mark in enumerate(reference_marks, start=1):
        reference_mark_content = reference_mark.Name
        reference_mark_content, _ = reference_mark_content.rsplit(" ", 1)
        _, _, reference_mark_data = reference_mark_content.split(" ", 2)

        data_dictionary = json.loads(reference_mark_data, strict=False)

        plain_cite = data_dictionary["properties"]["plainCitation"].strip()
        if data_dictionary["properties"].get("dontUpdate", False):
            raise NotImplementedError('Manually edited citation text is read-only: "%s" (delete and add again).' % plain_cite)
        if plain_cite[0] == '(' and plain_cite[-1] == ')':
            plain_cite = plain_cite[1:-1]

        if plain_cite[0] in "0123456789":
            insertZoteroBookmarks(True)
            plain_cite = plain_cite.replace("-", ",").replace("–", ",")
            numbers = list(plain_cite.split(","))

            for number in numbers:
                text_cursor = reference_mark.getAnchor()
                text_cursor_start = text_cursor.getStart()
                search_descriptor.SearchString = number
                found_range = document.findNext(text_cursor_start, search_descriptor)
                text_cursor_by_range = found_range.Text.createTextCursorByRange(found_range)
                text_cursor_by_range.HyperLinkURL = "#Ref_" + number
        else:
            insertZoteroBookmarks(False)

            if semicolon.search(plain_cite):
                names = list(plain_cite.split(";"))
            elif comma.search(plain_cite):
                names = list(plain_cite.split(","))
            elif colon.search(plain_cite):
                names = list(plain_cite.split(":"))
            else:
                names = [plain_cite]

            ref_count += len(data_dictionary['citationItems'])
            for citation_items in data_dictionary["citationItems"]:
                if "itemData" in citation_items.keys():
                    item_data = citation_items["itemData"]

                    author = item_data["author"][0]["family"]
                    date_year = item_data["issued"]["date-parts"][0][0]

                    citation = list(filter(lambda x: author in x, names))
                    citation_year_duplicates = [c for c in citation if (date_year in c)]

                    text_cursor = reference_mark.getAnchor()
                    text_cursor_start = text_cursor.getStart()

                    found = False
                    for cite in citation_year_duplicates:
                        search_descriptor.SearchString = cite
                        found_range = document.findNext(text_cursor_start, search_descriptor)
                        if found_range is None:
                            # TODO: display a prompt to fix the search pattern
                            # prompt: "Citation text not found for {author} {date_year}" (possibly customized). Fix the pattern:"
                            print('found_range is None at:\nReferenceMark: %s\nAnchor start=%s\nauthor: %s\ndate: %s\ncitation_year_duplicates: %s\ncite: %s' \
                                  % (reference_mark.Name, text_cursor.getStart(),
                                     author, date_year, citation_year_duplicates, cite))
                        if found_range.HyperLinkURL == "": #FIXME: found_range can be None
                            found = True
                            if found_range.String[-1].isalpha():
                                date_year = date_year + found_range.String[-1]
                            break
                        else:
                            text_cursor_start = found_range.getEnd()

                    if found is False:
                        # Occurs when:
                        # - it is already has a link.
                        # - or the displayed text was modified from the default.
                            
                        # Beware of comma-separated prefix, because it splits the citations into:
                        # names=["some prefix", " author et al.", " 2019"]
                        # causing empty match of citation_year_duplicates.
                        print('WARNING: text %r not found for author=%r date=%r citation=%r' \
                              % (citation_year_duplicates, author, date_year, plain_cite) )
                        #if citation_year_duplicates:
                            #continue  #FIXME: this makes things slow.
                        break

                    text_cursor_by_range = found_range.Text.createTextCursorByRange(found_range)
                    author_ref = unidecode(invalid_ref_chars.sub('', author))
                    text_cursor_by_range.HyperLinkURL = "#Ref_" + author_ref + "_" + date_year
    print('LO-zotero-hyperlink-citations: linkReferences terminated successfully (%d references, %d marks)' \
          % (ref_count, mark_count))


g_exportedScripts = linkReferences,
