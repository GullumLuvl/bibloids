#!/usr/bin/env python3

# Copyright © 2022 Gullumluvl <gullumluvlcodes[at]outlook[dot]com>
#
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See COPYING.txt or http://www.wtfpl.net/ for more details.


"""
Update a PDF file metadata by querying from the Zotero database.

Useful when PDFs are downloaded for temporary use, but not stored in Zotero.

Example: to sync to an e-reader, it usually requires to fix the metadata,
because even the Title and Author keys can be missing from official pdfs
from reputed journals...
"""

SEARCH_TERM_HELP = """SEARCH TERM HELP

There are two mutually exclusive ways to provide search terms:

1. implicit syntax:
    - a DOI alone (1 argument)
    - OR a creator and a date (2 arguments);
2. explicit syntax: "key=value" terms. Accepted keys are:
    - any valid Zotero field such as "date", "title", "DOI", "ISBN", "publicationTitle".
    - creator types: "creator" (i.e. no specific type), "author", "firstAuthor", "editor", "bookAuthor".

Creator syntax: lastname [, *firstnames].

Date syntax: year[-month[-day]]

EXAMPLES

    # print items matching "creator=Smith" AND "date=2022"
    ./pdfmetadata_from_zotero.py creator=Smith date=2022

    # more precise author and date
    ./pdfmetadata_from_zotero.py creator="Smith, John, Henry" date=2022-12

    # same as above, but implicit (use quoting to make exactly two arguments)
    ./pdfmetadata_from_zotero.py "Smith, John, Henry" 2022-12

    # search for a DOI
    ./pdfmetadata_from_zotero.py DOI=10.1136/bmj.38705.470590.55
    # or
    ./pdfmetadata_from_zotero.py 10.1136/bmj.38705.470590.55
"""


### Implementation notes
# The Zotero web api queries the *online* database. This is undesirable since
# we have a local database.

# Direct access to the SQLite database
# https://www.zotero.org/support/dev/client_coding/direct_sqlite_database_access


import sys
import re
import os.path as op
from collections import namedtuple
import argparse as ap
import subprocess as sp
import sqlite3
import logging
logger = logging.getLogger(__name__)


# To Edit PDF metadata:
# - See PyPDF2 (will however remove bookmarks)
# - pikepdf (coordinates XMP metadata with Pdfinfo metadata (the latter deprecated)
# - subprocess call to exiftool

# Some reminder on how to use the 'sqlite3' interactive shell:
#
# $ sqlite3 file:/home/$USER/Zotero/zotero.sqlite?immutable=1
#
# > .help
# > .databases
# > .tables

ZOTDIR = '~/Zotero'
CREATORS = ('author', 'editor', 'bookAuthor')

REGEX_DOI = re.compile(r'10\.[1-9][0-9]{2}[0-9.+]/\S+$')
REGEX_YEAR = re.compile(r'\d{1,4}$')

# Do not use the following as metadata field names, or any single letter name:
exitftool_OPTS = ('args', 'argFormat', 'charset', 'csv', 'dateFormat',
                  'forcePrint', 'escapeHTML', 'escapeXML',
                  'htmlDump', 'lang', 'long', 'php', 'short', 'veryShort', 'separator',
                  'sort', 'struct', 'tab', 'table', 'verbose',
                  'duplicates', 'composite', 'extractEmbedded', 'extension',
                  'fixBase', 'fast', 'fileOrder', 'ignore', 'if', 'ignoreMinorErrors',
                  'out', 'overwrite_original', 'overwrite_original_in_place',
                  'preserve', 'password', 'progress', 'quiet', 'recurse', 'scanForXMP',
                  'unknown', 'unknown2', 'writeMode', 'zip', 'pause',
                  'list', 'listx', 'listw', 'ver', 'geotag')
# Also see the options '-unknown' and '-list'

# Available PDF tags, see:
# - pdf info: https://exiftool.org/TagNames/PDF.html#Info
#   Author, CreationDate, Creator, Keywords, ModDate, Producer, Subject, Title, Trapped.
# - XMP pdfx (application-dependent): https://www.exiftool.org/TagNames/XMP.html#pdfx

def get_first(c, query):
    """c: connection cursor"""
    rows = list(c.execute(query))
    if len(rows) < 1:
        raise LookupError("No result from query: '%s'" % query)
    elif len(rows) > 1:
        print("WARNING: Multiple results from query '%s':" % query, file=sys.stderr)
        print('    ' + '\n    '.join(str(r) for r in rows), file=sys.stderr)
    return rows[0]

def quick_get_first(c, query):
    for row in c.execute(query):
        return row
        

def describe_zotDB(conn):
    """
    kwargs are valid Zotero database fields: DOI, ISBN, title,
    
    To search for author, you need 'creatorType=="author" && lastname=="Xyz"'
    """
    c = conn.cursor()
    # List tables
    print('TABLES:', ' '.join(sorted([row[0] for row in c.execute("SELECT name FROM sqlite_master WHERE type='table';")])))
    # "Describe" command:
    for tablename in ('fields', 'Creators', 'CreatorTypes', 'itemCreators', 'itemTypeCreatorTypes', 'items', 'itemData', 'itemDataValues', 'itemTypes', 'itemTypeFields', 'tags', 'itemTags'):
        print('#', tablename, ' (%d rows)' % list(c.execute('SELECT count(*) FROM %s;' % tablename))[0][0])
        colnames = []
        for row in c.execute('PRAGMA table_info("%s");' % tablename):
            colnames.append(row[1])
            #print('   ', row)
        print('   ', '\t'.join(colnames))
        for row in c.execute('SELECT * FROM %s LIMIT 5;' % tablename):
            print('   ', '\t'.join(str(x) for x in row))
        print()
    type_ids = dict(keyvalue for keyvalue in c.execute('SELECT typeName,itemTypeID FROM itemTypes;'))
    type_names = dict((value, key) for key, value in type_ids.items())
    print('type_names =', type_names)

        # Or: c.execute('...').fetchall()
        #


# The problem is that datetime.date requires month and day to be valid values
# This accepts any value, so month=0 means "month not set"
class ZoteroDate(object):
    def __init__(self, year, month=0, day=0):
        if not 0 <= month <= 12:
            raise ValueError('Invalid month number %s' % month)
        if not 0 <= day <= 31:
            raise ValueError('Invalid day number %s' % day)

        self.year = year
        self.month = month
        self.day = day

    @classmethod
    def parse(cls, rawdate):
        """
        Zotero dates are stored in double, examples:
            '2018-09-24 2018-09-24'
            '2018-09-24 2018
        and sometimes with zeros: '2018-00-00'
        """
        if not rawdate:
            return None
        
        return cls(*(int(x) for x in rawdate.split()[0].split('-')))

    def __iter__(self):
        yield self.year
        if self.month == 0:
            return
        yield self.month
        if self.day == 0:
            return
        yield self.day

    def __str__(self):
        return '-'.join('%02d' % x for x in self)

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__, ', '.join(str(x) for x in self))
    

def match_zotDB_items(conn, **kwargs):
    """
    Generator: yields one matching item at a time, structured as:
        (tagnames, item dictionary)

    - tagnames are valid variables associated to an item of the given type (article, thesis, etc);
    - item dictionary might lack values for some of the tags.
    
    Parameters:
    -----------

    - kwargs: keys should be valid Zotero database fields: DOI, ISBN, title, publicationTitle,
        and valid Zotero creator types: author, editor, bookAuthor...'
    """
    c = conn.cursor()

    # NOTE sqlite has the LIKE and REGEXP functions (and may have MATCH).
    
    # Setup the field that we want to show as output (=relevant biblio item infos)
    fields = ['date', 'title', 'DOI']  # expected IDs: date=6 title=1 DOI=58
    output_fields = list(CREATORS) + fields
    output_fields.append('type')  # itemType is special because in the items table. 
    type_fields = {'journalArticle': ['journalAbbreviation', 'volume', 'issue', 'publicationTitle'],
                   'book': ['ISBN', 'publisher', 'edition', 'series'],
                   'bookSection': ['bookTitle', 'publisher', 'edition', 'series'],
                   'thesis': ['university'],
                   'conferencePaper': ['proceedingsTitle', 'conferenceName', 'publisher', 'place'],
                   'report': ['institution'],
                   'preprint': ['repository']}
    # To allow for additional types:
    # Query the type:
    #     SELECT itemTypeID, typeName FROM itemTypes WHERE typeName=="preprint";
    # List all its fields:
    #     SELECT fieldID, fieldName FROM ItemTypeFields LEFT JOIN fields USING(fieldID) WHERE itemTypeID==38;

    # Investigate a specific item to understand the meaning of fields:
    # Search by DOI:
    #     SELECT itemID, I.itemTypeID FROM itemData D LEFT JOIN itemDataValues V USING(valueID) LEFT JOIN items I USING(itemID) WHERE D.fieldID==58 AND V.value=="10.48550/arXiv.2304.11826";
    # List fields
    #     SELECT * FROM itemData LEFT JOIN fields USING(fieldID) LEFT JOIN itemDataValues USING(valueID) WHERE itemID==6048;
    #
    type_field_union = list(set(tf for tfields in type_fields.values() for tf in tfields)) 

    # Setup converters for the numerical encoding of field and itemType in the DB
    field_ids = dict(keyvalue for keyvalue in c.execute('SELECT fieldName,fieldID FROM fields;'))
    field_names = dict((value, key) for key,value in field_ids.items())

    type_ids = dict(keyvalue for keyvalue in c.execute('SELECT typeName,itemTypeID FROM itemTypes;'))
    type_names = dict((value, key) for key, value in type_ids.items())
    logger.debug('type_names = %s', type_names)

    fieldIDs = tuple(field_ids[field] for field in fields + type_field_union)
    creatortypeIDs = dict(c.execute('SELECT creatorType,creatorTypeID'
                                  ' FROM CreatorTypes'
                                  ' WHERE CreatorType IN %s;' % (CREATORS,)))

    def datefilter(itemdict): return True

    # Search the item based on the given kwargs
    if 'DOI' in kwargs:
        fid = field_ids['DOI']
        resultIDs = [
            get_first(c,
                'SELECT itemID,I.itemTypeID'
                ' FROM itemData D'
                '    LEFT JOIN itemDataValues V USING(valueID)'
                '    LEFT JOIN items I USING(itemID)'
                ' WHERE D.fieldID==%d AND V.value=="%s";' % (fid, kwargs['DOI']))
            ]
    else:
        if 'date' in kwargs:
            date_elems = [int(x) for x in kwargs['date'].split('-')] # year, [month, day]
            def datefilter(itemdict):
                itemdate = itemdict.get('date')
                return all(elem == item_elem for elem,item_elem in zip(date_elems, itemdate))

        if set(('creator', 'firstAuthor') + CREATORS).intersection(kwargs):

            creatortypes = {ctype: ctypeid for ctype, ctypeid in
                    c.execute('SELECT creatorType, creatorTypeID '
                              'FROM creatorTypes '
                              'WHERE creatorType IN (%s);' % ','.join(map(repr, CREATORS)))}

            creator_selection = []  # store the creators that should all be found in the searched item.

            for ctype in set(('creator', 'firstAuthor') + CREATORS).intersection(kwargs):
                last, *surnames = [name.strip() for name in kwargs[ctype].split(',')]
                creator_query = ('SELECT creatorID,lastName,firstName '
                                'FROM Creators WHERE lastName=="%s"' % last)
                if surnames:
                    creator_query += ' AND firstName=="%s"' % ' '.join(surnames)

                # FIXME: return all possible authors (date can be used to disambiguate)
                creator_ids, creator_names = [], []
                # The following query may yield multiple creators for a same name, if too common.
                for a_id, *name in c.execute(creator_query + ';'):
                    creator_ids.append(a_id)
                    creator_names.append(tuple(name))
                logger.info('%d creator matches: %s', len(creator_names),
                    ', '.join('%s %s' % name for name in creator_names))
                
                creator_selection.append('(IC.creatorID IN (%s)' % ','.join(map(str, creator_ids)))
                if ctype == 'firstAuthor':
                    creator_selection[-1] += 'AND creatorTypeID==%d AND orderIndex==0)' % creatortypes['author']
                elif ctype != 'creator':
                    creator_selection[-1] += 'AND creatorTypeID==%d)' % creatortypes[ctype]
                else:
                    creator_selection[-1] += ')'

            # Build the item query with a condition on all requested creators
            item_query = ('SELECT itemID,I.itemTypeID'
                ' FROM itemCreators IC LEFT JOIN items I USING(itemID)'
                ' WHERE ') + ' OR '.join(creator_selection) + ';'
            logger.debug('final item query: %r', item_query)
            resultIDs = list(c.execute(item_query))
            

    if set(kwargs).difference(CREATORS + ('creator', 'firstAuthor', 'DOI', 'date')):
        raise NotImplementedError('Search terms not accepted yet.')

    # We got the IDs of the matching items, now display its data
    for rid,tid in resultIDs:
        
        # Retrieve the useful fields of this single item
        itemdict = dict(c.execute(
            #'SELECT fieldID,valueID FROM itemData WHERE itemID==%d AND fieldID IN %s' % (rid, output_fieldIDs)
            'SELECT fields.fieldName,itemDataValues.value'
            ' FROM itemData LEFT JOIN fields USING(fieldID)'
            ' LEFT JOIN itemDataValues USING(valueID)'
            ' WHERE itemData.itemID==%d AND fields.fieldID IN %s;' % (rid, fieldIDs)
            ))
        # Retrieve zotero tags (excluding user-defined tags):
        itemdict['tags'] = ', '.join(
                        row[0] for row in c.execute(
                        'SELECT tags.name FROM itemTags '
                        'JOIN tags USING(tagID) '
                        'WHERE itemTags.itemID==%s AND itemTags.type==1' % rid))

        # Retrieve the creators
        for ctype in CREATORS:
            creators = list(c.execute(
                'SELECT C.lastName,C.firstName'
                ' FROM itemCreators IC LEFT JOIN Creators C USING(creatorID)'
                ' WHERE IC.creatorTypeID==%d AND IC.itemID==%d'
                ' ORDER BY IC.orderIndex;' % (creatortypeIDs[ctype], rid)))
            if creators:
                itemdict[ctype] = ', '.join('%s %s' % a for a in creators)
        itemdict['type'] = type_names[tid]
        itemdict['date'] = ZoteroDate.parse(itemdict['date'])
        #datavalues = dict(c.execute('SELECT valueID,value FROM itemDataValues WHERE valueID IN %s' % tuple(data[1] for data in itemdata) ))
        #itemdict = {field_names[data[0]]: datavalues[data[1]] for data in}
        if datefilter(itemdict):
            yield output_fields + type_fields[type_names[tid]], itemdict
        
        
def parse_search_terms(terms):
    """Accepted formats:
        1. list of "key=value" terms;
        2. a bare DOI;
        3. author, year;
    """
    args = {}
    try:
        for term in terms:
            key, value = term.split('=', 1)
            args[key] = value
    except ValueError:
        if len(terms) == 1 and REGEX_DOI.match(terms[0]):
            args['DOI'] = terms[0]
        elif len(terms) == 2 and REGEX_YEAR.match(terms[1]):
            args['author'] = terms[0]
            args['date'] = terms[1]
        else:
            raise ValueError('Invalid search format')

    return args


ZOT_2_PDFINFO = {'editor': 'producer', 'tags': 'keywords'}

# How to assemble Zotero tags into the new PDF metadata entries:
ITEMTYPE_OUT_SUBJECT = {
            'journalArticle':  ['publicationTitle', 'volume', 'issue'],
            'book':            ['series', 'publisher'],
            'conferencePaper': ['subject', 'conferenceName'],
            'bookSection':     ['bookTitle', 'series'],
            'preprint': ['repository']
            }


def assemble_metadata(tagnames, itemdict):
    for tag in ('title', 'author', 'date', 'DOI', 'editor', 'tags'):
        try:
            value = itemdict[tag]
        except KeyError:
            continue
        if value is None:
            continue
        #if tag.lower() not in available_tags:
        #    print('WARNING: unknown tag %r (to exiftool)' % tag, file=sys.stderr)
        if '"' in value:
            value = value.replace('"', '\\"')
        yield ZOT_2_PDFINFO.get(tag, tag), value

    subject_values = []
    for tag in ITEMTYPE_OUT_SUBJECT.get(itemdict['type'], []):
        try:
            value = itemdict[tag]
        except KeyError:
            continue
        if '"' in value:
            value = value.replace('"', '\\"')
        if tag == 'volume':
            value = 'vol. ' + value
        subject_values.append(value)

    if subject_values:
        yield 'subject', ' - '.join(subject_values)
    

def main():
    logging.basicConfig(format=logging.BASIC_FORMAT)

    parser = ap.ArgumentParser(description=__doc__, epilog=SEARCH_TERM_HELP,
                               formatter_class=ap.RawDescriptionHelpFormatter)
    parser.add_argument('-f', '--file', help='PDF file to update [If none, print the matching biblio items]')
    parser.add_argument('-n', '--dryrun', action='store_true',
                        help='Do not update metadata. Only query Zotero and print the exiftool command.')
    parser.add_argument('-m', '--max-authors', metavar='N', type=int,
                        help='shorten author list with "et al." [NOT IMPLEMENTED]')
    parser.add_argument('terms', nargs='*',
                        help=('Search terms (see SEARCH TERM HELP).'
                        ' If no arguments, describe the database.'))
    args = parser.parse_args()

    #If zotero is running, the DB is locked. '?mode=ro' does not work, only '?immutable'
    dbpath = op.join(op.expanduser(ZOTDIR), 'zotero.sqlite?immutable=1')

    logger.info('DB: %s Exists: %s', dbpath, op.exists(dbpath.rsplit('?', 1)[0]))
    with sqlite3.connect('file:' + dbpath, uri=True) as conn:
        if not args.terms:
            describe_zotDB(conn)
            return

        number_matches = 0
        for tagnames, itemdict in match_zotDB_items(conn, **parse_search_terms(args.terms)):
            number_matches += 1
            
            # We only use the first biblio match so exit now, but warn about extra matches.
            if number_matches > 1:
                print('WARNING: Multiple corresponding matches.', file=sys.stderr)
                break

            print('\n'.join('  %s: %s' % (tag, itemdict.get(tag)) 
                for tag in tagnames) + '\n')

            if args.file:
                cmd = ['exiftool']
                cmd_check_exist = ['exiftool']
                for newtag, value in assemble_metadata(tagnames, itemdict):
                    cmd_check_exist.append('-' + newtag)
                    cmd.append('-%s=%s' % (newtag, value))
                cmd.append(args.file)
                cmd_check_exist.append(args.file)

                # Check if some metadata would be overwritten
                r = sp.run(cmd_check_exist, stderr=sp.PIPE, stdout=sp.PIPE)
                if r.returncode != 0 or r.stderr:
                    print('ERROR: Return code=%s  Error message:\n' + r.stderr.decode() + '\n', file=sys.stderr)
                elif r.stdout:
                    print('WARNING: the following metadata will be overwritten:\n' + r.stdout.decode() + '\n', file=sys.stderr)

                if args.dryrun:
                    print('DRYRUN:cmd:', '\n    '.join(repr(arg) for arg in cmd), file=sys.stderr)
                else:
                    r = sp.check_call(cmd)
        if not number_matches:
            print('NO MATCH!', file=sys.stderr)
                

if __name__ == '__main__':
    main()
